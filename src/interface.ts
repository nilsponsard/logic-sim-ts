/**
 *  This module contains the functions to use the interface, can be used by a module
 *
 * @category tools
 */
module interfaceMaker {
    let interfaceOutputContent = <HTMLElement>document.getElementById("interfaceOutputContent")
    let interfaceOutput = <HTMLElement>document.getElementById("interfaceOutput")
    let interfaceOutputBackButton = <HTMLButtonElement>document.getElementById("interfaceOutputButtonBack")

    /**
     * show via an interface an element
     * @param element html element to show
     * @param callback callback if needed, triggered when closing the message
     */
    export function outputHTML(element: HTMLElement | Node, callback?: () => void) {
        interfaceOutputContent.innerHTML = ""
        interfaceOutput?.classList.remove("hidden")
        interfaceOutputContent?.appendChild(element)
        if (callback) {

            interfaceOutputBackButton.onclick = function () {
                interfaceOutput?.classList.add("hidden")
                callback()
            }
        } else {
            interfaceOutputBackButton.onclick = function () {
                interfaceOutput?.classList.add("hidden")
            }
        }
    }

    export function hideCurrentInterface() {
        interfaceOutput?.classList.add("hidden")

    }
}
