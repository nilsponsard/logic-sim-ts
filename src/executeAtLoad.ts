/**
 * Things to execute when all the scripts are loaded
 *  @category editor
 */
module executeAtLoad {
    editor.refresh()

    editorInterfaceElements.setMove()
    editorInterfaceElements.showNewInterface()
}
