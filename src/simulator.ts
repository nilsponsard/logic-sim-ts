/**
 *
 * @category circuit
 */
module simulator {


    export class Simulator {
        connectors: Array<links.Connector>
        elements: Array<logicGates.Element>
        currentId: number

        constructor() {
            this.connectors = []
            this.elements = []
            this.currentId = 0
        }

        getElementByPos(position: utils.Vec2d): logicGates.Element | null {
            let result = null
            this.elements.forEach((value, index) => {
                if (utils.collidePointRect(position, value.position, value.size)) {
                    result = value
                }
            })
            return result
        }

        deleteAtPos(position: utils.Vec2d) {
            let result = null
            let connectorsToDelete: Array<number> = []
            let tolerance = 1
            // search for connectors
            this.connectors.forEach((currConnector, connectorIndex) => {
                for (let i = 0; i <= currConnector.pathPoints.length; i++) {
                    let point1: utils.Vec2d
                    if (i === 0) point1 = currConnector.input.position
                    else point1 = currConnector.pathPoints[i - 1]

                    let point2
                    if (i === currConnector.pathPoints.length) point2 = currConnector.output.position
                    else point2 = currConnector.pathPoints[i]

                    let distMouse = position.distanceTo(point1) + position.distanceTo(point2)
                    let distPoints = point1.distanceTo(point2)
                    if (distPoints + tolerance > distMouse && distPoints - tolerance < distMouse) {
                        connectorsToDelete.push(currConnector.id)
                        currConnector.output.setState(false, nodes.createNewUpdateID())
                        delete currConnector.input
                        delete currConnector.output
                        delete this.connectors[connectorIndex]
                        return
                    }
                }

            })
            this.elements.forEach((value, index) => {
                if (utils.collidePointRect(position, value.position, value.size)) {
                    result = index
                }
                value.inputNodes.forEach(currINode => {
                    if (utils.collidePointRect(position, currINode.position, nodes.nodeSize)) {
                        result = index
                    }
                })
                value.outputNodes.forEach(currONode => {
                    if (utils.collidePointRect(position, currONode.position, nodes.nodeSize)) {
                        result = index
                    }
                })
            })
            if (result != null) {
                let element = this.elements[result]
                element.outputNodes.forEach((value) => {
                    value.setState(false, nodes.createNewUpdateID())
                })
                this.connectors.forEach((value, index) => {
                    element.outputNodes.forEach((node, i) => {
                        if (node == value.input) {
                            connectorsToDelete.push(value.id)
                            delete this.connectors[index]
                        }

                    })
                    element.inputNodes.forEach((node, i) => {
                        if (node == value.output) {
                            connectorsToDelete.push(value.id)
                            delete this.connectors[index]
                            node.setState(false, nodes.createNewUpdateID())
                        }

                    })

                })
                delete element.outputNodes
                delete element.inputNodes

                delete this.elements[result]
            }
            connectorsToDelete.forEach(connectorToDelete => {
                this.elements.forEach(currentElement => {
                    currentElement.outputNodes.forEach(node => {
                        node.updateFunctions.delete(connectorToDelete)
                    })
                })
            })

        }

        handleClick(position: utils.Vec2d): logicGates.Element | nodes.OutputNode | nodes.InputNode | null {
            let result = null
            this.elements.forEach((value) => {
                if (utils.collidePointRect(position, value.position, value.size)) {
                    let center = new utils.Vec2d(value.position.x + value.size.x / 2, value.position.y + value.size.y / 2)
                    if (value instanceof interactions.Button && center.distanceTo(position) < value.radius) {

                        value.toggle()
                    } else {
                        result = value
                    }

                }
                value.inputNodes.forEach(currINode => {
                    if (utils.collidePointRect(position, currINode.position, nodes.nodeSize)) {
                        result = currINode
                    }
                })
                value.outputNodes.forEach(currONode => {
                    if (utils.collidePointRect(position, currONode.position, nodes.nodeSize)) {
                        result = currONode
                    }
                })
            })
            return result
        }

        draw(ctx: CanvasRenderingContext2D) {
            this.connectors.forEach(value => {
                value.draw(ctx)
            })
            this.elements.forEach(value => {
                value.draw(ctx)
            })
        }

        connect(input: nodes.InputNode, output: nodes.OutputNode, connectorPath: Array<utils.Vec2d>) {

            let connector = new links.Connector(input, output, connectorPath, ++this.currentId)
            this.connectors.push(connector)
            return connector

        }


        addElement(g: logicGates.Element) {
            g.id = ++this.currentId
            this.elements.push(g)

            return g
        }

    }

    export let simulator = new Simulator()


}