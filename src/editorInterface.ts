/**
 *
 * @category editor
 */
module editorInterfaceElements {
    let openInterface = document.createElement("div")
    {
        let label = document.createElement("label")
        label.innerText = "open from file : "
        let inputFile = document.createElement("input")
        inputFile.type = "file"
        inputFile.id = "openSave"
        inputFile.addEventListener("change", openFile)
        label.htmlFor = inputFile.id
        let p1 = document.createElement("p")
        p1.appendChild(label)
        p1.appendChild(inputFile)
        openInterface.appendChild(p1)
        let createNewButton = document.createElement("button")
        createNewButton.innerText = "Create New"
        createNewButton.addEventListener("click", createNew)
        openInterface.appendChild(createNewButton)
    }

    function createNew() {
        simulator.simulator = new simulator.Simulator()
        interfaceMaker.hideCurrentInterface()

    }

    function openFile(this: HTMLInputElement, e: Event) {
        if (this.files) {
            if (this.files.length > 0) {
                let file = this.files[0]
                if (file) {
                    let reader = new FileReader()
                    reader.addEventListener("load", (e) => {
                        if (e.target?.result) {
                            let result = e.target.result.toString()
                            editor.loadFromTxt(result)
                            interfaceMaker.hideCurrentInterface()
                        }
                    })
                    reader.readAsText(file)
                }
            }
        }

    }


    class ElementSelectorInterface {
        private _html: HTMLElement
        currentList: Array<logicGates.Element>
        generatedList: Array<logicGates.Element>
        button: HTMLButtonElement

        constructor(list: Array<logicGates.Element>, buttonId: string) {
            this._html = document.createElement("div")
            this.currentList = list
            this.generatedList = []
            this.button = <HTMLButtonElement>document.getElementById(buttonId)

            this.generate()
            this.button.addEventListener("click", () => {
                this.show()
            })
        }

        get html(): HTMLElement {
            if (this.currentList != this.generatedList) this.generate()
            return this._html
        }

        show() {
            interfaceMaker.outputHTML(this.html)
        }

        generate() {
            this._html = document.createElement("div")
            this.currentList.forEach((value) => {
                let cv = document.createElement("canvas")
                cv.width = value.size.x + logicGates.defaultPosition.x * 2
                cv.height = value.size.y + logicGates.defaultPosition.y * 2
                let ctx = <CanvasRenderingContext2D>cv.getContext("2d")
                value.draw(ctx)
                cv.addEventListener("click", (e) => {
                    editor.grabbed = simulator.simulator.addElement(
                        value.clone()
                    )
                    editor.refresh()
                    interfaceMaker.hideCurrentInterface()
                })
                this._html.appendChild(cv)
            })
            this.generatedList = this.currentList.slice()
        }
    }

    let simpleGates = new ElementSelectorInterface(
        logicGates.simpleGates,
        "simpleGates"
    )
    let interactionsI = new ElementSelectorInterface(
        interactions.available,
        "interaction"
    )

    export let hitbox = <HTMLElement>document.getElementById("cvHitbox")
    let deleteButton = <HTMLButtonElement>document.getElementById("delete")
    let moveButton = <HTMLButtonElement>document.getElementById("move")
    let saveButton = <HTMLButtonElement>document.getElementById("save")
    let newButton = <HTMLButtonElement>document.getElementById("new")
    newButton.addEventListener("click", showNewInterface)

   export function showNewInterface() {
        interfaceMaker.outputHTML(openInterface)
    }

    export function setMove() {
        deleteButton.classList.remove("selected")
        moveButton.classList.add("selected")
        editor.mode = editor.modes.move
    }

    export function setDelete() {
        deleteButton.classList.add("selected")
        moveButton.classList.remove("selected")
        editor.mode = editor.modes.delete
    }

    deleteButton.addEventListener("click", setDelete)
    moveButton.addEventListener("click", setMove)
    saveButton.addEventListener("click", () => {
        editor.save()
    })
}
