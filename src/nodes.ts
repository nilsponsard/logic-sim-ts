/**
 * This module contains the definitions for InputNode and OutputNode
 * A node is a connexion point for linking two [[Element]] together,
 * the base class is {@link IONode}.
 * - Inputs are instances of {@link InputNode} and should be placed at the left of the Element.
 * - Outputs are instances of {@link OutputNode} and should be placed at the right of the Element.
 * @category circuit
 */

module nodes {
    let currNodeId = 0
    export type simpleCallback = (updateID: number) => any // need to pass already updated gates to prevent recursions loops

    let currentUpdateId = 0

    export function createNewUpdateID() {
        console.log("created new id")
        return ++currentUpdateId
    }

    /**
     * The size a Node should have
     */
    export let nodeSize = new utils.Vec2d(10, 10)

    class IONode {
        /**
         * Functions to call when the state is changed by {@link setState}
         */
        updateFunctions: Map<number, simpleCallback> // map id to the callback
        state: boolean
        position: utils.Vec2d
        offset: utils.Vec2d
        id: number
        lastUpdadteID: number

        /**
         *
         * @param position
         * @param offset the offset to apply to the position of the Node
         * @param updateFunctionInitMap
         */
        constructor(position: utils.Vec2d, offset: utils.Vec2d, updateFunctionInitMap?: Map<number, simpleCallback>,) {
            this.position = position.clone()
            this.offset = offset
            this.state = false
            this.id = ++currNodeId
            this.updateFunctions = new Map<number, simpleCallback>()
            if (updateFunctionInitMap)
                this.updateFunctions = updateFunctionInitMap
            this.move(position)
            this.lastUpdadteID = -1
        }

        getState() {
            return this.state
        }

        addUpdateCallback(callback: simpleCallback, elementId: number) {
            this.updateFunctions.set(elementId, callback)
        }

        setState(state: boolean, updateID: number) {

            if (this.lastUpdadteID != updateID) {
                this.lastUpdadteID = updateID
                console.log(this.lastUpdadteID, updateID)
                this.state = state
                this.updateFunctions.forEach((callback, key) => {

                    callback(updateID)
                })
            }


        }

        /**
         * Move to the position and apply the {@link offset}
         * @param position where to move
         */
        move(position: utils.Vec2d) {
            this.position = position.clone().add(this.offset)
        }

        draw(ctx: CanvasRenderingContext2D) {
            ctx.beginPath()
            ctx.strokeStyle = "#000000"
            ctx.fillStyle = "#fafafa"
            ctx.lineWidth = 1
            ctx.rect(this.position.x, this.position.y, nodeSize.x, nodeSize.y)
            if (this.state) {
                ctx.fillStyle = "#ffff00"
            }

            ctx.fill()
            ctx.stroke()
            ctx.closePath()
        }


    }


    /**
     * InputNode, should be placed at the left of an {@link Element}
     */
    export class InputNode extends IONode {
        type = "input"

    }

    /**
     * InputNode, should be placed at the right of an {@link Element}
     */
    export class OutputNode extends IONode {
        type = "output"
    }
}