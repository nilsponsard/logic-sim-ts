/**
 * In this Module all the canvas events are handled
 * @category editor
 */
module editor {
    export let canvas = <HTMLCanvasElement>document.getElementById("canvas")
    export let context = <CanvasRenderingContext2D>canvas.getContext("2d")
    export enum modes {
        delete,
        move,
    }

    export enum pathModes {
        horizontal,
        vertical,
    }

    export let currentPathMode = pathModes.horizontal

    export let debugActivated = true
    export function debug(s: string) {
        if (debugActivated) {
            console.log(s)
        }
    }


    export let sticky = true
    export let stickySize = nodes.nodeSize.x



    let connectorPath: Array<utils.Vec2d> = []
    export let mode = modes.move

    let oldMouseCvPos = new utils.Vec2d(0, 0)
    let translateOffset = new utils.Vec2d(0, 0)

    export let grabbed: null | logicGates.Element = null
    let tempToConnect: null | nodes.InputNode | nodes.OutputNode = null

    export function refresh() {
        canvas.width = window.innerWidth
        canvas.height = window.innerHeight - 105

        context.setTransform(1, 0, 0, 1, translateOffset.x, translateOffset.y);
        // console.log(translateOffset)
        simulator.simulator.draw(context)
    }

    function stickyCheck(pos: utils.Vec2d) {
        if (sticky) {
            pos.x = Math.round(pos.x / stickySize) * stickySize + stickySize / 2
            pos.y = Math.round(pos.y / stickySize) * stickySize + stickySize / 2
        }

        return pos
    }

    function computeConnectorPath(pos: utils.Vec2d) {
        let out = pos.clone()
        let relativePos = pos.clone()
        debug("computeConnectorPath")
        if (tempToConnect) {

            let distanceX = Math.abs(relativePos.x - connectorPath[connectorPath.length - 1].x)
            let distanceY = Math.abs(relativePos.y - connectorPath[connectorPath.length - 1].y)
            if (distanceX > distanceY) {
                currentPathMode = pathModes.horizontal
                relativePos.y = connectorPath[connectorPath.length - 1].y
            } else {
                currentPathMode = pathModes.vertical
                relativePos.x = connectorPath[connectorPath.length - 1].x
            }
            context.beginPath()
            context.strokeStyle = links.defaultStrokeStyle
            context.moveTo(
                tempToConnect.position.x + nodes.nodeSize.x / 2,
                tempToConnect.position.y + nodes.nodeSize.y / 2
            )
            connectorPath.forEach((value) => {
                context.lineTo(value.x, value.y)
            })
            context.lineTo(relativePos.x, relativePos.y)
            context.stroke()
            context.closePath()
            out = relativePos
        }

        return out
    }

    canvas.addEventListener("mousedown", (e) => {
        let CVpos = new utils.Vec2d(e.offsetX, e.offsetY)
        let relativePos = CVpos.substract(translateOffset)
        relativePos = computeConnectorPath(relativePos)
        if (mode === modes.move && e.button == 0) {
            let selected = simulator.simulator.handleClick(relativePos)
            if (selected instanceof logicGates.Element) {
                grabbed = selected
                tempToConnect = null
            } else if (selected instanceof nodes.InputNode && selected.type == "input") {
                if (tempToConnect) {
                    if (tempToConnect instanceof nodes.OutputNode &&
                        tempToConnect.type == "output") {

                        simulator.simulator.connect(
                            tempToConnect,
                            selected,
                            connectorPath.slice()
                        )

                        tempToConnect.setState(
                            tempToConnect.getState(),
                            nodes.createNewUpdateID()
                        )
                        tempToConnect = null
                        connectorPath = []
                    }
                } else {
                    tempToConnect = selected
                    relativePos = stickyCheck(relativePos)
                    connectorPath.push(stickyCheck(selected.position.clone()))
                    currentPathMode = pathModes.vertical
                }
            } else if (selected instanceof nodes.OutputNode &&
                selected.type == "output") {
                if (tempToConnect) {
                    if (tempToConnect instanceof nodes.InputNode &&
                        tempToConnect.type == "input") {
                        let path = connectorPath.slice()
                        path.reverse()
                        simulator.simulator.connect(selected, tempToConnect, path)
                        let tempArr: Array<number> = []
                        selected.setState(selected.getState(), nodes.createNewUpdateID())
                        tempToConnect = null
                        connectorPath = []
                    }
                } else {
                    tempToConnect = selected
                    relativePos = stickyCheck(relativePos)
                    connectorPath.push(stickyCheck(selected.position.clone())) // stichyCheck to get centered
                    currentPathMode = pathModes.vertical
                }
            } else {
                relativePos = stickyCheck(relativePos)

                if (tempToConnect) {
                    switch (currentPathMode) {
                        case editor.pathModes.horizontal:
                            relativePos.y = connectorPath[connectorPath.length - 1].y
                            break
                        case editor.pathModes.vertical:
                            relativePos.x = connectorPath[connectorPath.length - 1].x
                            break
                    }
                    connectorPath.push(relativePos.clone())
                }
            }
        } else if (mode == modes.delete) {
            // editorInterfaceElements.setMove()
            simulator.simulator.deleteAtPos(relativePos)
        }
        CVpos = stickyCheck(CVpos)
        oldMouseCvPos = CVpos
        refresh()
        // computeConnectorPath(relativePos)
    })
    canvas.addEventListener("mousemove", (e) => {
        let CVpos = new utils.Vec2d(e.offsetX, e.offsetY)
        let relativePos = CVpos.substract(translateOffset)
        relativePos = stickyCheck(relativePos)
        if (grabbed) {
            relativePos.x -= grabbed.size.x / 2
            relativePos.y -= grabbed.size.y / 2
            grabbed.move(new utils.Vec2d(relativePos.x, relativePos.y))
            grabbed.draw(context)
            editorInterfaceElements.setMove()
        } else if (e.buttons == 4) {
            let offset = CVpos.substract(oldMouseCvPos)
            translateOffset = translateOffset.add(offset)
        }


        refresh()
        computeConnectorPath(relativePos)
        oldMouseCvPos = CVpos

    })
    canvas.addEventListener("mouseup", (e) => {
        let CVpos = new utils.Vec2d(e.offsetX, e.offsetY)
        let relativePos = CVpos.substract(translateOffset)
        if (e.button == 0) {
            grabbed = null
        }
        refresh()
        computeConnectorPath(relativePos)

    })
    canvas.addEventListener("contextmenu", (e) => {
        e.preventDefault()
        if (tempToConnect) {
            tempToConnect = null
            connectorPath = []
        }
    })

    export function save() {
        let txt = JSON.stringify(simulator.simulator)
        let file = new Blob([txt], { type: "application/javascript" })
        let a = document.createElement("a")
        let url = URL.createObjectURL(file)
        a.href = url
        a.style.display = "none"
        a.download = "logicCircuit.json"
        document.body.appendChild(a)
        a.click()
        setTimeout(function () {
            document.body.removeChild(a)
            window.URL.revokeObjectURL(url)
        }, 0)
    }

    export function loadFromTxt(txt: string) {
        let data = {}
        data = JSON.parse(txt)
        let newSim = new simulator.Simulator()

        try {
            Object.assign(newSim, data)
        } catch (e) {
            console.error(e)

        }
        console.log(newSim)

    }
}
