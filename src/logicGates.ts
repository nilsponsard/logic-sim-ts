/**
 *
 * @category circuit
 */

module logicGates {
    import nodeSize = nodes.nodeSize;
    export let defaultGateSize = new utils.Vec2d(3 * nodes.nodeSize.x, 3 * nodes.nodeSize.y)
    export let defaultFontPx = 20
    export let defaultFont = "monospace"
    export let defaultFontColor = "#000000"
    export let defaultPosition = new utils.Vec2d(20, 20)

    export const minGateSize = new utils.Vec2d(3 * nodes.nodeSize.x, 3 * nodes.nodeSize.y)

    /*interface Element {
       position: utils.Vec2d,
       size: utils.Vec2d,

       inputNodes: Array<nodes.InputNode>,
       outputNodes: Array<nodes.OutputNode>,

       update(): void,

       draw(ctx: CanvasRenderingContext2D): void,

       move(position: utils.Vec2d): void
   }*/


    export abstract class Element {
        position: utils.Vec2d
        size: utils.Vec2d
        inputs: number
        outputs: number
        inputNodes: Array<nodes.InputNode>
        outputNodes: Array<nodes.OutputNode>
        id: number
        title: string
        fontPx: number


        protected constructor(position: utils.Vec2d, inputs = 2, outputs = 1, title = "", size?: utils.Vec2d, id = -1) {
            this.inputs = inputs
            this.outputs = outputs
            this.position = position
            this.id = id
            this.size = nodeSize.clone()
            if (size)
                this.size = size
            else {
                this.size.y = Math.max((Math.max(inputs, outputs) * 2 + 1) * nodes.nodeSize.y, minGateSize.y)
                this.size.x = Math.max(Math.floor((title.length * (defaultFontPx / 1.7) + 10) / nodes.nodeSize.x) * nodes.nodeSize.x, minGateSize.x) // found by trying some values
            }
            this.fontPx = defaultFontPx

            this.title = title
            let x = -nodes.nodeSize.x
            let y = 0
            let stepY
            this.inputNodes = []
            if (inputs == 1) {
                stepY = (this.size.y - nodes.nodeSize.y) / 2
                let nodeOffset = new utils.Vec2d(x, stepY)
                let node = new nodes.InputNode(this.position, nodeOffset, new
                    Map<number, nodes.simpleCallback>().set(this.id, (updateID: number) => {
                        this.update(updateID)
                    })
                )
                this.inputNodes.push(node)
                console.log(this.inputNodes, this.inputs)
            } else {
                stepY = (this.size.y - nodes.nodeSize.y) / (this.inputs - 1)
                for (let i = 0; i < this.inputs; i++) {
                    let nodeOffset = new utils.Vec2d(x, y)
                    let node = new nodes.InputNode(this.position, nodeOffset, new Map<number, nodes.simpleCallback>().set(this.id, (updateID: number) => {
                        this.update(updateID)
                    }))
                    this.inputNodes.push(node)
                    y += stepY
                }
            }

            this.outputNodes = []
            x = this.size.x
            stepY = (this.size.y - nodes.nodeSize.y) / (this.outputs * 2)
            y = stepY
            for (let i = 0; i < this.outputs; i++) {
                let nodeOffset = new utils.Vec2d(x, y)
                let node = new nodes.OutputNode(this.position, nodeOffset)
                this.outputNodes.push(node)
                y += stepY
            }
        }

        abstract clone(): Element

        abstract update(updateID: number): void

        move(position: utils.Vec2d) {
            this.position.copy(position)
            this.inputNodes.forEach((value => {
                value.move(position)
            }))
            this.outputNodes.forEach((value => {
                value.move(position)
            }))
        }

        draw(ctx: CanvasRenderingContext2D) {

            this.inputNodes.forEach((value => {
                value.draw(ctx)
            }))
            this.outputNodes.forEach((value => {
                value.draw(ctx)
            }))

            ctx.strokeStyle = "#242424"
            ctx.lineWidth = 2
            ctx.strokeRect(this.position.x, this.position.y, this.size.x, this.size.y)

            ctx.fillStyle = defaultFontColor
            ctx.font = `${this.fontPx}px ${defaultFont}`
            ctx.textAlign = "left"
            ctx.fillText(this.title, this.position.x + 5, this.position.y + this.size.y / 2 + this.fontPx / 3)
        }


    }

    export class GateFromTruthTable extends Element {
        truthTableInputs: Array<Array<boolean>>
        truthTableOutputs: Array<Array<boolean>>

        // title: string

        constructor(position: utils.Vec2d, truthTableInputs: Array<Array<boolean>>, truthTableOutputs: Array<Array<boolean>>, title: string) {
            if (truthTableInputs.length < 1) throw new Error("can't take a truthTableInputs array with 0 length")
            if (truthTableOutputs.length < 1) throw new Error("can't take a truthTableOutputs array with 0 length")
            if (truthTableInputs.length != truthTableOutputs.length) throw new Error("the two parts of the truth table must have the same number of rows")

            let inputs = truthTableInputs[0].length
            let outputs = truthTableOutputs[0].length
            // let size = defaultGateSize.clone()
            // size.y = (Math.max(inputs, outputs) * 2 + 1) * nodes.nodeSize.y
            // size.x = title.length * (defaultFontPx / 1.7) + 10 // found by trying some values

            super(position, inputs, outputs, title);

            this.truthTableInputs = truthTableInputs
            this.truthTableOutputs = truthTableOutputs
            // this.title = title
            this.update(nodes.createNewUpdateID())
        }

        clone(): logicGates.Element {
            return new GateFromTruthTable(this.position.clone(), utils.cloneNestedArray(this.truthTableInputs), utils.cloneNestedArray(this.truthTableOutputs), this.title.slice());
        }

        draw(ctx: CanvasRenderingContext2D) {
            super.draw(ctx);

        }

        update(updateID: number) {
            let foundIndex = -1

            for (let index = 0; index < this.truthTableInputs.length; index++) {
                let current = this.truthTableInputs[index];
                let valid = true
                for (let i = 0; i < current.length; i++) {
                    if (this.inputNodes[i].getState() != current[i]) {
                        valid = false
                        break
                    }
                }
                if (valid) {
                    foundIndex = index
                    break
                }
            }
            if (foundIndex != -1) {
                for (let i = 0; i < this.truthTableOutputs[foundIndex].length; i++) {
                    this.outputNodes[i].setState(this.truthTableOutputs[foundIndex][i], updateID)
                }
            } else {
                for (let i = 0; i < this.outputNodes.length; i++) {
                    this.outputNodes[i].setState(false, updateID)
                }
            }
        }


    }


    export function convertTruthTableToBool(truthTable: Array<Array<number>>) {
        let out: Array<Array<boolean>> = []
        truthTable.forEach(value => {
            let line: Array<boolean> = []
            value.forEach(value1 => {
                if (value1 == 1) line.push(true)
                else line.push(false)
            })
            out.push(line)
        })
        return out
    }

    export class NotGate extends GateFromTruthTable {
        constructor(position: utils.Vec2d) {
            let truthInput = [
                [0],
                [1]
            ]
            let truthOutput = [
                [1],
                [0]
            ]

            super(position, convertTruthTableToBool(truthInput), convertTruthTableToBool(truthOutput), "/1");
        }

    }

    export class AndGate extends GateFromTruthTable {
        constructor(position: utils.Vec2d) {
            let truthInput = [
                [0, 0],
                [0, 1],
                [1, 0],
                [1, 1]
            ]
            let truthOutput = [
                [0],
                [0],
                [0],
                [1]
            ]

            super(position, convertTruthTableToBool(truthInput), convertTruthTableToBool(truthOutput), "&");
        }

    }

    export class NAndGate extends GateFromTruthTable {
        constructor(position: utils.Vec2d) {
            let truthInput = [
                [0, 0],
                [0, 1],
                [1, 0],
                [1, 1]
            ]
            let truthOutput = [
                [1],
                [1],
                [1],
                [0]
            ]

            super(position, convertTruthTableToBool(truthInput), convertTruthTableToBool(truthOutput), "/&");
        }

    }

    export class OrGate extends GateFromTruthTable {
        constructor(position: utils.Vec2d) {
            let truthInput = [
                [0, 0],
                [0, 1],
                [1, 0],
                [1, 1]
            ]
            let truthOutput = [
                [0],
                [1],
                [1],
                [1]
            ]
            super(position, convertTruthTableToBool(truthInput), convertTruthTableToBool(truthOutput), "≥1");
        }
    }

    export class XOrGate extends GateFromTruthTable {
        constructor(position: utils.Vec2d) {
            let truthInput = [
                [0, 0],
                [0, 1],
                [1, 0],
                [1, 1]
            ]
            let truthOutput = [
                [0],
                [1],
                [1],
                [0]
            ]

            super(position, convertTruthTableToBool(truthInput), convertTruthTableToBool(truthOutput), "=1");
        }

    }

    /**
     * here are gates considered as simple Gate, you can add here gates and it will be displayed in the `Simple gates` menu
     */
    export let simpleGates: Array<Element> = [
        new NotGate(defaultPosition),
        new OrGate(defaultPosition),
        new AndGate(defaultPosition),
        new NAndGate(defaultPosition),
        new XOrGate(defaultPosition)
    ]


}

/**
 *
 * @category circuit
 */
module interactions {
    export class Button extends logicGates.Element {
        radius: number

        constructor(position: utils.Vec2d, size?: utils.Vec2d) {
            super(position, 0, 1, "", size);
            this.radius = this.size.x / 4
        }

        draw(ctx: CanvasRenderingContext2D) {
            super.draw(ctx);
            ctx.beginPath()
            ctx.strokeStyle = "#000000"
            ctx.fillStyle = "none"
            ctx.lineWidth = 2
            ctx.fillStyle = "#950707"

            ctx.arc(this.position.x + this.size.x / 2, this.position.y + this.size.y / 2, this.radius, 0, Math.PI * 2)
            if (this.outputNodes[0].getState()) ctx.fill()
            ctx.stroke()
            ctx.closePath()
        }

        clone(): logicGates.Element {
            return new Button(this.position.clone(), this.size.clone());
        }

        update() {
        }

        toggle() {
            this.outputNodes[0].setState(!this.outputNodes[0].getState(), nodes.createNewUpdateID())
        }
    }

    export class Lamp extends logicGates.Element {
        radius: number

        constructor(position: utils.Vec2d) {
            super(position, 1, 0);
            this.radius = this.size.x / 4

        }

        draw(ctx: CanvasRenderingContext2D) {
            super.draw(ctx);
            ctx.beginPath()
            ctx.strokeStyle = "#000000"
            ctx.fillStyle = "none"
            ctx.lineWidth = 2
            ctx.fillStyle = "#ffa700"

            ctx.arc(this.position.x + this.size.x / 2, this.position.y + this.size.y / 2, this.radius, 0, Math.PI * 2)
            if (this.inputNodes[0].getState()) ctx.fill()
            ctx.stroke()
            ctx.closePath()
        }

        update() {
        }

        clone(): logicGates.Element {
            return new Lamp(this.position.clone());
        }
    }

    /**
     * here are gates considered as interaction Elements, you can add here gates and it will be displayed in the `Interaction Elements` menu
     */
    export let available: Array<logicGates.Element> = [
        new Button(logicGates.defaultPosition),
        new Lamp(logicGates.defaultPosition)
    ]

}


/**
 *
 * @category circuit
 */
namespace links {
    export interface Link {
        pathPoints: Array<utils.Vec2d>

        draw(ctx: CanvasRenderingContext2D): void
    }

    export let defaultStrokeStyle = "#000000"

    export class Connector implements Link {

        activatedStyle = "#ff0000"
        lineWidth = 2
        id: number
        input: nodes.InputNode
        output: nodes.OutputNode
        pathPoints: Array<utils.Vec2d>

        constructor(input: nodes.OutputNode, output: nodes.InputNode, pathPoints?: Array<utils.Vec2d>, id = -1) {
            this.id = id
            this.pathPoints = []
            if (pathPoints) this.pathPoints = pathPoints
            this.input = input
            this.output = output
            this.input.addUpdateCallback((updateID: number) => {
                this.update(updateID)
            }, this.id)
        }

        update(updateID: number) {
            this.output.setState(this.input.getState(), updateID)
        }

        draw(ctx: CanvasRenderingContext2D): void {
            ctx.beginPath()
            ctx.moveTo(this.input.position.x + nodes.nodeSize.x / 2, this.input.position.y + nodes.nodeSize.y / 2)
            this.pathPoints.forEach(value => {
                ctx.lineTo(value.x, value.y)
            })

            ctx.lineTo(this.output.position.x + nodes.nodeSize.x / 2, this.output.position.y + nodes.nodeSize.y / 2)
            ctx.lineWidth = this.lineWidth
            ctx.strokeStyle = defaultStrokeStyle
            if (this.input.getState()) ctx.strokeStyle = this.activatedStyle
            ctx.stroke()
            ctx.closePath()
        }
    }
}
