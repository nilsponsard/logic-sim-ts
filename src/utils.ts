/**
 * This module contains useful functions for the app
 * @category tools
 */
module utils {
    /**
     * Clone a nested array so nothing is referenced, this is for 2D array
     * @param arg the array to clone
     */
    export function cloneNestedArray<T>(arg: Array<Array<T>>): Array<Array<T>> {
        let out: Array<Array<T>> = []
        arg.forEach(currentLine => {
            let outLine: Array<T> = []
            currentLine.forEach(value => {
                outLine.push(value)
            })
            out.push(outLine)
        })
        return out;
    }

    /**
     * Clone an array, equivalent to array.slice()
     * @param arg the array to clone
     */
    export function cloneArray<T>(arg: Array<T>): Array<T> {
        let out: Array<T> = []
        arg.forEach(value => {
            out.push(value)
        })
        return out
    }


    /**
     * Simple class to use 2D vectors, used in positions and sizes
     */
    export class Vec2d {
        x: number
        y: number

        /**
         * @param x the x attribute
         * @param y the y attribute
         */
        constructor(x: number, y: number) {
            this.x = x
            this.y = y
        }

        /**
         * Copy a vector into this vector
         * @param v the vector to copy
         */
        copy(v: Vec2d) {
            this.x = v.x
            this.y = v.y
            return this
        }

        /**
         * set the x and y of the vector
         * @param x
         * @param y
         */
        goto(x: number, y: number) {
            this.x = x
            this.y = y
            return this
        }

        /**
         * return an object with the sames attributes but without any reference
         */
        clone(): Vec2d {
            return new Vec2d(this.x, this.y)
        }

        /**
         * add a vector
         * @param v Vector to add
         */
        add(v: Vec2d): Vec2d {
            let temp = this.clone()
            temp.x += v.x
            temp.y += v.y
            return temp
        }

        substract(v: Vec2d): Vec2d {
            let temp = this.clone()
            temp.x -= v.x
            temp.y -= v.y
            return temp
        }

        /**
         * get the distance between this vector and the vector in parameter, used in positions
         * @param v the vector to compare
         */
        distanceTo(v: Vec2d): number {
            let temp = this.clone()
            temp = temp.substract(v)
            return Math.sqrt(temp.x * temp.x + temp.y * temp.y)
        }

    }

    /**
     * Check if there is a collision with the rect
     *
     * @param point
     * @param rectPos
     * @param rectSize
     */
    export function collidePointRect(point: Vec2d, rectPos: Vec2d, rectSize: Vec2d) {
        let collide = true
        if (point.x < rectPos.x) collide = false
        if (point.y < rectPos.y) collide = false
        if (point.x > rectPos.x + rectSize.x) collide = false
        if (point.y > rectPos.y + rectSize.y) collide = false
        return collide
    }

    export function collideRectRect(rect1Pos: Vec2d, rect1Size: Vec2d, rect2Pos: Vec2d, rect2Size: Vec2d) {
        // TODO : create function

    }
}
